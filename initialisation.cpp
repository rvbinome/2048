/*
    initialisation.cpp

    TP 2048

    Auteurs : Tuckey David, Cadet Armand
    Date : 14/09/2016
*/
#ifndef INCLUDE
#include"all.h"
#define INCLUDE
#endif

void initialisationJeu( int tab[MSIZE][MSIZE], float p) {
    // Initialise le tableau avec seulement des 0, et remplis deux cases
    for ( int i = 0; i< MSIZE; i++ ) { // on parcout les lignes
        for ( int j = 0; j< MSIZE; j++ ) {// on parcout les colonnes
            tab[i][j] = 0; // on initialise à 0
        }
    }

    //On génère deux valeurs
    genererNouveauNombre(tab, p);
    genererNouveauNombre(tab, p);
}
