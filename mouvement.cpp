/*
    mouvement.cpp

    TP 2048

    Auteurs : Tuckey David, Cadet Armand
    Date : 14/09/2016
*/
#ifndef INCLUDE
#include"all.h"
#define INCLUDE
#endif

int moverightcell(int plateau[MSIZE][MSIZE], int i, int j, int &maxi, bool &modified) {
    // Fonction qui va bouger ou fusionner une case vers la droite

	int score = 0;
	//Si on est sur une case qui ne peut pas/plus bouger
	if (j >= MSIZE-1 || j >= maxi) {
		return 0;
	}
	//Si la case à droite est vide et que la case courante est vide
	if (plateau[i][j+1] == 0 and plateau[i][j] != 0) {
		//On déplace la case à droite
		plateau[i][j+1] = plateau[i][j];
		plateau[i][j] = 0;
		modified = true;
		//Puis on recommence sur la case à droite
		score += moverightcell(plateau, i, j+1, maxi, modified);
	}
	//Si la case à droite est identique et que la case courante est vide
	else if (plateau[i][j+1] == plateau[i][j] and plateau[i][j+1] != 0) {
		//On additionne avec la case à droite
		plateau[i][j+1]+=plateau[i][j];
		score += plateau[i][j+1];
		plateau[i][j] = 0;
		modified = true;
		//On ne modifie plus les cases à droite de celle-ci
		maxi = j;
		//score += moverightcell(plateau, i, j+1, maxi, modified);
	}
	return score;
}

int moveright(int plateau[MSIZE][MSIZE], bool &modified, float p) {
    //Effectue le mouvement vers la droite
	int score = 0;
	//On parcourt les lignes puis les cellules, de droite vers la gauche
	for(int i = 0; i<MSIZE; i++) {
		int maxi = MSIZE-1;
		for(int j = MSIZE-1; j>=0; j--) {
			score += moverightcell(plateau, i, j, maxi, modified);
		}
	}

	// Si il ya a eu un mouvement on génère une nouvelle valeur
    if ( modified)
        genererNouveauNombre(plateau, p);

	return score;
}

void symmetry(int plateau[MSIZE][MSIZE], int tab[MSIZE][MSIZE]) {
    // Fait le symétrique de plateau dans tab
	//On parcourt les lignes en les symétrisant
	for(int i = 0; i<MSIZE; i++) {
        for ( int j=0; j<MSIZE; j++) {
            tab[i][j] = plateau[i][MSIZE-1-j];
        }
	}
}

int moveleft(int plateau[MSIZE][MSIZE], bool &modified, float p) {
    // Fait le mouvement vers la gauche
    int tab[MSIZE][MSIZE];
	symmetry(plateau, tab); // on fait le symétrique du plateau
	int score = moveright(tab, modified, p); // on bouge le mouvement du symétrique vers la droite
	symmetry(tab, plateau); // on remet le plateau dans le bon sens
	return score;
}

int moveup(int plateau[MSIZE][MSIZE], bool &modified, float p) {
    // fait le mouvement vers le haut
    int score = 0;
    int tab[MSIZE][MSIZE];
    transpose(plateau, tab); // on transpose le tableau
	symmetry(tab, plateau); // on fait le symétrique du plateau
	score = moveright(plateau, modified, p); // on bouge le mouvement du symétrique vers la droite
	symmetry(plateau, tab); // on remet le plateau dans le bon sens
    transpose(tab, plateau); // on transpose

    return score;

}

int movedown(int plateau[MSIZE][MSIZE], bool &modified, float p) {
    // fait le mouvement vers le bas
    int score = 0;
    int tab[MSIZE][MSIZE];
    transpose(plateau, tab); // on transpose le tableau
    score = moveright(tab, modified, p);
    transpose(tab, plateau); //On met le tableau dans le bon sens

    return score;

}

void transpose(int tab1[MSIZE][MSIZE] , int tab2[MSIZE][MSIZE]) {
    //Transpose tab1 dans tab2
    for ( int i =0; i< MSIZE; i++) { // on parcours les lignes
        for ( int j=0; j<MSIZE; j++ ) { // on parcours les colonnes
            tab2[j][i] = tab1[i][j]; // on transpose
        }
    }
}

bool canPlay( int tab[MSIZE][MSIZE] ) {
    // Vérifie si il y a un mouvement possible
    bool retour = false;


    for ( int i =0; i< MSIZE; i++) {  // On parcour chaque ligne
        for ( int j=0; j<MSIZE; j++ ) { // on parcour chaque colones
            if ( j > 0) { // si j-1 est dans le tableau
                if (tab[i][j] == tab[i][j-1]) // si i,j-1 est la même case que i,j
                    retour = true; // mouvement possible
            }
             if ( i > 0) {// si i-1 est dans le tableau
                if (tab[i][j] == tab[i-1][j])// si i-1,j est la même case que i,j
                    retour = true;// mouvement possible
            }
             if ( j < MSIZE -1 ) {// si j+1 est dans le tableau
                if (tab[i][j] == tab[i][j+1])// si i,j+1 est la même case que i,j
                    retour = true;// mouvement possible
            }
            if ( i < MSIZE -1) {// si i+1 est dans le tableau
                if (tab[i][j] == tab[i+1][j])// si i+1,j est la même case que i,j
                    retour = true;// mouvement possible
            }
            if ( tab[i][j] == 0 ) {// si la case i,j est vide
                retour = true;// mouvement possible
            }
        }
    }
    return retour;

}
