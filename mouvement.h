/*
    mouvement.h

    TP 2048

    Auteurs : Tuckey David, Cadet Armand
    Date : 14/09/2016
*/
int moverightcell(int plateau[MSIZE][MSIZE], int i, int j, int &maxi, bool &modified);
int moveright(int plateau[MSIZE][MSIZE], bool &modified, float p);
void symmetry(int plateau[MSIZE][MSIZE]);
int moveleft(int plateau[MSIZE][MSIZE], bool &modified, float p);
int movedown(int plateau[MSIZE][MSIZE], bool &modified, float p);
int moveup(int plateau[MSIZE][MSIZE], bool &modified, float p);
void transpose(int tab1[MSIZE][MSIZE] , int tab2[MSIZE][MSIZE]);
bool canPlay( int tab[MSIZE][MSIZE] );
