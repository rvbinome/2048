/*
    aleatoire.cpp

    TP 2048

    Auteurs : Tuckey David, Cadet Armand
    Date : 14/09/2016
*/
#ifndef INCLUDE
#include"all.h"
#define INCLUDE
#endif

bool estVide_notsecure( int tab[MSIZE][MSIZE], int i, int j) {
    // Vérifie si la case i,j du tableau tab est nulle
    return tab[i][j] == 0;
}

int genererValeur( float p ) {
    // Génère une valeur entre 2 et 4 avec une proba p
    int al = rand() % 101; // obtention d'un nombre entre 0 et 100
    if ( al < p * 100) {
        return 4;
    }else {
        return 2;
    }
}

bool genererNouveauNombre(int tab[MSIZE][MSIZE], float p) {
    // génère un nouveau nombre dans le tableau

    int vide[MSIZE*MSIZE][2]; // tableau contenant les coordonnées de toutes les cases vides
    int nbre_vide = 0; // Compteur du nombre de cases vides
    int choix;
    for ( int i = 0; i< MSIZE; i++ ) { // on parcourt les lignes
        for ( int j = 0; j< MSIZE; j++ ) { // on parcourt les colonnes

            if ( estVide_notsecure( tab, i, j ) ) { // si la case i,j est vide on l'ajoute au tableau vide
                vide[nbre_vide][0] = i; // ajout de la coordonné i
                vide[nbre_vide][1] = j; // ajout de la coordonné j
                nbre_vide++; // incrémentation du nombre de case vide

            }

        }
    }

    if ( nbre_vide > 0 ) { // Si il y a des cases vide
        choix = rand() % nbre_vide; // on prend une case vide aléatoirement
        tab[vide[choix][0]][vide[choix][1]] = genererValeur(p); // on ajoute la valeur au tableau
        return true;
    }else {
        return false;
    }
}



