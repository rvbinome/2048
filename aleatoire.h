/*
    aleatoire.h

    TP 2048

    Auteurs : Tuckey David, Cadet Armand
    Date : 14/09/2016
*/

bool genererNouveauNombre(int tab[MSIZE][MSIZE],  float p );
bool estVide_notsecure( int tab[MSIZE][MSIZE], int i, int j);
int genererValeur( float p );
