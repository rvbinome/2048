/*
    all.h

    TP 2048

    Auteurs : Tuckey David, Cadet Armand
    Date : 14/09/2016
*/

//Réalise tous les includes nécessaires

#include "constante.h"
#include <iostream>
#include <cstdlib>
#include <time.h>
#include <algorithm>
#include "affichage.h"
#include "initialisation.h"
#include "mouvement.h"
#include "aleatoire.h"


using namespace std;
