/*
    Affichage.cpp

    TP 2048

    Auteurs : Tuckey David, Cadet Armand
    Date : 14/09/2016
*/

#ifndef INCLUDE
#include"all.h"
#define INCLUDE
#endif

void affichageTableau ( int tab[MSIZE][MSIZE] ) {
    // Affiche un tableau d'entiers
    for ( int i =0; i< MSIZE; i++) { // On parcourt les lignes
        for ( int j=0; j<MSIZE; j++ ) { // on parcourt chaque colonne
            cout << tab[i][j] << "\t"; // On affichge la valeur
        }
        cout << endl;
    }
    cout<<endl;
}

int getMax( int tab[MSIZE][MSIZE] ) {
    // Renvoie la valeur maximale d'un tableau d'entiers
    int maxi = 0; // initialisation du maximum
    for ( int i =0; i< MSIZE; i++) { // on parcourt les lignes
        for ( int j=0; j<MSIZE; j++ ) { // on parcourt les colonnes
            if ( tab[i][j] > maxi) { // Si la valeur de la case est superieure au maximum temporaire
                maxi = tab[i][j]; // on change la valeur du maximum
            }
        }
    }
    return maxi;
}
