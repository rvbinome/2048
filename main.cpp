/*
    main.cpp

    TP 2048

    Auteurs : Tuckey David, Cadet Armand
    Date : 14/09/2016
*/

#ifndef INCLUDE
#include"all.h"
#define INCLUDE
#endif


int main()
{
    srand(time(NULL)); // initialisation pour les nombres aléatoires
    float p = 0.25; // initialisation de la probabilité

    // initlisation du tableau
    int tab[MSIZE][MSIZE];
    initialisationJeu(tab, p);

    bool play = true;
    int score = 0;
    int action = 0;
    bool modified = false;
    while ( play ) { // tant que l'on peut faire un mouvement
        action = 0; // Va permettre le choix de l'action par l'utilisateur
        modified = false;
        cout << "Score : " <<score <<endl<<endl;
        affichageTableau(tab); // affiche le tableau

        // Donne les instructions
        cout << "Que voulez vous faire ?" <<endl;
        cout << "\t" << "1" << "\t"<<endl;
        cout << "2\t" << "\t" << "3"<<endl;
        cout << "\t" << "4" << "\t"<<endl;
        cin >> action;

        if ( action == 1 ) {  // vers le haut
            score += moveup(tab, modified, p);
        }else if ( action == 2 ) { // vers la gauche
            score += moveleft(tab, modified, p);
        }else if ( action == 3 ) { // vers la droite
            score += moveright(tab, modified, p);
        }else if ( action == 4 ) { // vers le bas
            score += movedown(tab, modified, p);
        }

        if ( getMax(tab) == 2048 or !canPlay(tab) ) { // si la partie est finie
            play = false;
        }

    }


    affichageTableau(tab);
    if ( getMax(tab) == 2048 ) { // si il a gagné
        cout << "Bravo vous avez gagné"<<endl;
    }else { // si il a perdu
        cout << "Try again" <<endl;
    }

    return 0;
}
